# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* **set() - initialize**
    * 透過 getElementById 將 canvas 取出，方便之後操作。
    * ctx - 引用 getContext，便可以直接透過 ctx 呼叫各種內建函式。
    * 利用 addEventlistener 偵測滑鼠的各種行為。
* **color(obj) - color selector**
    * 用一個 switch 來判斷被選擇的顏色，依據 obj.id (也就是我給選取的圖示定義的id)。
    * 更改 global 變數 "col" 以更換顏色。
* **size(obj) - brush size selector**
    * 與上面的 color 寫法相同，唯更改的變數由 "col" 變成 "wid"。
* **font_select() - font style selector**
    * 宣告一個變數 val 並用 getElementById 取得下拉選單當前被選擇的項目。
    * 與前面的 color, size 相似，使用 switch 更換字型與文字大小 (更改 global 變數 "font")。
* **button(obj) - function selector for buttons**
    * 所有 button 被點擊後，會先進入這個函式。
    * 一樣是使用 switch 來判斷吃進來的 obj 的 id，決定接下來要執行的動作。
    * save - 創建一個物件 my，使用 download 與 doDataURL 函式，使圖片得以下載到使用者的裝置。
    * clear - 使用 clearRect 函式，將整塊 canvas 清空。
    * pushsh(), undodo(), redodo() - 在 report 最底下。
* **draw(e) - active when mouse is moving**
    * 首先用 ctx.strokeStyle 與 ctx.lineWidth 套用文字與筆刷設定。
    * 根據當前升起的 flag，決定接下來要做的事：
        * brush - 使用內建函式 (lineTo, moveTo) 實現畫線的動作。
        * eraser - 使用 arc 與 fill 函式，將滑鼠位置方圓 8px 清空。
        * spray(additional) - 透過畫出大量的實心圓(半徑可調整)，做出類似噴漆的效果。
        * circle, rect, tri - 透過 global 變數 start_x 與 start_y 記住點擊時的滑鼠位置。
    * global 變數 x, y 用來追蹤當前的滑鼠位置。
* **down() - mousedown event**
    * 將 global 變數 drawing 與 clicked 設為 true，在其他函式中它們將作為一些動作的判斷依據。
    * x, y 一樣用來追蹤當前的滑鼠位置。
* **cancel(e) - mouseup event**
    * 首先將 drawing 設為 false，代表滑鼠現在已經不在點擊狀態。
    * 將 x, y 設為 NaN，以避免下次點擊時還記著上一次的位置。
    * 根據當前升起的 flag，決定接下來要做的事：
        * circle - 使用上面的 start_x, start_y 與 Math 中的 sqrt 來計算半徑並畫圓 (arc)。
        * rect - 以 start_x 與 start_y，以及當前的滑鼠座標為四個參考點畫出矩形 (fillRect)。
        * tri - 使用三次 lineTo 函式畫出三個邊，再用 fill 函式將三角形填滿。
        * 這邊的 cnt 是用來記當前的進度 (undo, redo那邊會用到)。
        * 滑鼠提起代表完成一個動作，因此用 toDataURL() 將進度存進 global array - imgs 中。
* **out() - mouseout event**
    * 將 drawing 設為 false。
    * 將 out 與 cancel 分開來寫是為了避免在畫形狀時超出範圍 (直接視為不合法的使用方式)。
    * x, y 一樣要設為 NaN。
    * 基本上這個函式跟 cancel 沒什麼兩樣，只是去除了一些我不想看到的情況 (使用者應該也不樂見)。
* **底下的 brush() 一直到 tri() 就是一些 flag 的升降，我給每個功能一個 flag 方便整合**
* **text_mode - text input event**
    * 在這個模式中，賦予 canvas 一個 onclick function - addInput。
    * 在 addInput 函式中，寫入當前的座標，並且創建新物件 "input"。
    * 設定好一些基本參數後，使用 onkeydown 使滑鼠點擊畫面時，呼叫下一個函式 handleEnter。
    * 在 handleEnter 中，如果偵測到 Enter 鍵被按下，使用 fillText 將剛剛輸入的文字貼到畫面上。
    * hasInput 是用來控制文字方塊的數量，不同時出現超過一個。
* **pushsh(), undodo(), redodo() - undo/redo functions**
    * 基本上就是去 google，然後自己照著打一遍發現可以用就結束了，沒什麼原創性。
    * 總之就是開一個 array 去存每次 mouseup 時的狀態，然後像 stack 那樣子使用。
* **handleImage(e) - upload image function**
    * 一樣是 google，然後自己照打。
    * 使用了內建的 FileReader 型別，配合 ctx 的 drawImage 與 FileReader 的 readAsDataURL 來上傳圖片。
    * FileReader 太神啦。