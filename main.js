var canvas, ctx;
var x, x2, y, y2;
var cursor = "default",
    col = "black",
    wid = 2;
drawing = false;
brush_flag = true,
    eraser_flag = false,
    spray_flag = false,
    text_flag = false,
    circle_flag = false,
    rect_flag = false,
    tri_flag = false,
    clicked = false,
    start_x = 0,
    start_y = 0,
    font = '14px Arial',
    hasInput = false;

function set() {
    canvas = document.getElementById("paper");
    ctx = canvas.getContext("2d");
    canvas_height = canvas.height;
    canvas_width = canvas.width;
    canvas.addEventListener("mousemove", draw);
    canvas.addEventListener("mousedown", down);
    canvas.addEventListener("mouseup", cancel);
    canvas.addEventListener("mouseout", out);
    imgs.push(canvas.toDataURL());
    cnt++;
}

function color(obj) {
    switch (obj.id) {
        case "green":
            col = "green";
            break;
        case "blue":
            col = "blue";
            break;
        case "red":
            col = "red";
            break;
        case "yellow":
            col = "yellow";
            break;
        case "orange":
            col = "orange";
            break;
        case "black":
            col = "black";
            break;
        case "white":
            col = "white";
            break;
    }
}

function size(obj) {
    switch (obj.id) {
        case "px2":
            wid = 2;
            break;
        case "px3":
            wid = 3;
            break;
        case "px4":
            wid = 4;
            break;
        case "px5":
            wid = 5;
            break;
        case "px6":
            wid = 6;
            break;
    }
}

function font_select() {
    var val = document.getElementById("fonts").value;
    switch (val) {
        case "Arial":
            font = '14px Arial';
            break;
        case "ArIaL":
            font = '22px Arial';
            break;
        case "ARIAL":
            font = '30px Arial';
            break;
        case "Courier New":
            font = '14px Courier New';
            break;
        case "CoUrIeR NeW":
            font = '22px courier new';
            break;
        case "COURIER NEW":
            font = '30px courier new';
            break;
        case "Lucida Console":
            font = '14px Lucida Console';
            break;
        case "LuCiDa CoNsOlE":
            font = '22px Lucida Console';
            break;
        case "LUCIDA CONSOLE":
            font = '30px Lucida Console';
            break;
        case "Times New Roman":
            font = '14px Times New Roman';
            break;
        case "TiMeS NeW RoMaN":
            font = '22px Times New Roman';
            break;
        case "TIMES NEW ROMAN":
            font = '30px Times New Roman';
            break;
    }
}

function button(obj) {
    switch (obj.id) {
        case "save":
            var my = document.createElement('a');
            my.download = "myMasterpiece.png";
            my.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
            my.click();
            break;
        case "clear":
            ctx.clearRect(0, 0, canvas_width, canvas_height);
            cnt = 0;
            break;
        case "undo":
            undodo();
            break;
        case "redo":
            redodo();
            break;
        case "upload":
            upload();
            break;
    }
}

function draw(e) {
    if (!drawing) return;
    ctx.strokeStyle = col;
    ctx.lineWidth = wid;
    if (brush_flag) {
        ctx.globalCompositeOperation = "source-over";
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.closePath();
    }
    if (eraser_flag) {
        ctx.globalCompositeOperation = "destination-out";
        ctx.beginPath();
        ctx.arc(x, y, 8, 0, Math.PI * 2, false);
        ctx.fill();
    }
    if (spray_flag) {
        ctx.fillStyle = col;
        ctx.globalCompositeOperation = "source-over";
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.arc(x, y, wid, 0, Math.PI * 2, false);
        ctx.fill();
    }
    if (circle_flag || rect_flag || tri_flag) {
        ctx.fillStyle = col;
        ctx.globalCompositeOperation = "source-over";
        if (clicked) {
            ctx.beginPath();
            start_x = e.offsetX;
            start_y = e.offsetY;
            ctx.moveTo(start_x, start_y);
            clicked = false;
        }
    }
    x = e.offsetX;
    y = e.offsetY;
}

function down() {
    drawing = true;
    clicked = true;
    x = e.offsetX;
    y = e.offsetY;
}

function cancel(e) {
    drawing = false;
    x = NaN;
    y = NaN;
    if (circle_flag) {
        var delta_x = e.offsetX - start_x,
            delta_y = e.offsetY - start_y;
        ctx.arc(start_x, start_y, Math.sqrt(delta_x * delta_x + delta_y * delta_y), 0, Math.PI * 2, false);
        ctx.fill();
        ctx.closePath();
    }
    if (rect_flag) {
        ctx.fillRect(start_x, start_y, e.offsetX - start_x, e.offsetY - start_y);
        ctx.closePath();
    }
    if (tri_flag) {
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineTo(e.offsetX, start_y);
        ctx.moveTo(e.offsetX, start_y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.fill();
        ctx.closePath();
    }
    cnt++;
    if (cnt < imgs.length) {
        imgs.length = cnt;
    }
    imgs.push(canvas.toDataURL());
}

function out() {
    drawing = false;
    x = NaN;
    y = NaN;
}

function brush() {
    brush_flag = true;
    eraser_flag = false;
    spray_flag = false;
    text_flag = false;
    circle_flag = false;
    rect_flag = false;
    tri_flag = false;
    canvas.style.cursor = "default";
    canvas.onclick = function () { };
}

function eraser() {
    brush_flag = false;
    eraser_flag = true;
    spray_flag = false;
    text_flag = false;
    circle_flag = false;
    rect_flag = false;
    tri_flag = false;
    canvas.style.cursor = "move";
    canvas.onclick = function () { };
}

function spray() {
    brush_flag = false;
    eraser_flag = false;
    spray_flag = true;
    text_flag = false;
    circle_flag = false;
    rect_flag = false;
    tri_flag = false;
    canvas.style.cursor = "move";
    canvas.onclick = function () { };
}

function text() {
    brush_flag = false;
    eraser_flag = false;
    spray_flag = false;
    text_flag = true;
    circle_flag = false;
    rect_flag = false;
    tri_flag = false;
    canvas.style.cursor = "text";
    text_mode();
}

function circle() {
    brush_flag = false;
    eraser_flag = false;
    spray_flag = false;
    text_flag = false;
    circle_flag = true;
    rect_flag = false;
    tri_flag = false;
    canvas.style.cursor = "crosshair";
    canvas.onclick = function () { };
}

function rect() {
    brush_flag = false;
    eraser_flag = false;
    spray_flag = false;
    text_flag = false;
    circle_flag = false;
    rect_flag = true;
    tri_flag = false;
    canvas.style.cursor = "crosshair";
    canvas.onclick = function () { };
}

function tri() {
    brush_flag = false;
    eraser_flag = false;
    spray_flag = false;
    text_flag = false;
    circle_flag = false;
    rect_flag = false;
    tri_flag = true;
    canvas.style.cursor = "crosshair";
    canvas.onclick = function () { };
}

function text_mode() {
    var text_x, text_y;
    if (text_flag) {
        canvas.onclick = function (e) {
            if (hasInput) return;
            text_x = e.offsetX;
            text_y = e.offsetY;
            addInput(e.clientX, e.clientY);
        }
        function addInput(a, b) {
            var input = document.createElement('input');
            input.type = 'text';
            input.style.position = 'fixed';
            input.style.left = (a - 4) + 'px';
            input.style.top = (b - 4) + 'px';
            input.onkeydown = handleEnter;
            document.body.appendChild(input);
            input.focus();
            hasInput = true;
        }
        function handleEnter(e) {
            var keyCode = e.keyCode;
            if (keyCode === 13) {
                ctx.font = font;
                ctx.fillText(this.value, text_x - 5, text_y + 10);
                document.body.removeChild(this);
                hasInput = false;
            }
        }
    }
}

var imgs = new Array();
var cnt = -1;

function pushsh() {
    var dataurl = canvas.toDataURL();
    imgs[cnt] = dataurl;
    if (cnt != 99) cnt++;
    else cnt = 0;
    if (max < cnt) max = cnt;
}

function undodo() {
    if (cnt > 0) {
        cnt--;
        var canvasPic = new Image();
        canvasPic.src = imgs[cnt];
        canvasPic.onload = function () {
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}

function redodo() {
    if (cnt < imgs.length - 1) {
        cnt++;
        var canvasPic = new Image();
        canvasPic.src = imgs[cnt];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

var imageLoader = document.getElementById('file');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}